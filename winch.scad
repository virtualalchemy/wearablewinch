
//safety tolerance used around moving parts. increase if the moving parts are grinding against each other. don't change otherwise.
safety = .2;
//the thickness to use for most of the walls
wallthickness = 2;
//the radius to use for the tightening knob
winchradius = 25/2;
//the height to use for the tightening knob
winchheight = 20;
//the radius to use for the screw that secures the tightening knob
screwradius = 4/2;
//the number of teeth on the tightening knob
winchteeth = 20;
//the radius for the hole in the tightening knob that the wire goes through
winchwireholeradius = 2/1;
//how wide to make the teeth
teethheight = wallthickness*2;
//how long to make the teeth
teethlength = wallthickness*2;
//how high the bolt that secures the tightening screw is
boltheight = 4;
//the radius of the wire tightening knob bolt
boltradius = 10/2;

//the length of the arm that the winching catch rides on. use a bigger number for stiffer material.
wincharmlength = 40;
//the thickness of the arm that the winching catch rides on. use a smaller number for stiffer material.
wincharmthickness = 1;
//the length of the lever to release the winching catch
winchpaddlelength = 20;

//the height of the platform the tightening knob rides on
winchwasherheight = wallthickness;
//the length of the winching mech plate
winchplatelength = 50;
//the width of the winching mech plate
winchplatewidth = 45;

wireholeplatethickness = wallthickness*2;

function sidelength(radius, sides) = 2*(radius*(sin(180/sides)));

function apothemlength(radius, sides) = cos(180/sides)*radius;

module tooth(length, height)
{
	polygon([[0,0],[0,height],[length,0]]);
}

module gear(teeth, innerradius, outerradius)
{
	increment = 360.0/teeth;
	rotate([0,0,-increment/2])
	//regular_polygon(teeth, innerradius);
	circle(r = innerradius);
	echo(increment);
	sidel = sidelength(innerradius, teeth);
	for (i = [0:teeth])
	{
		rotate([0,0,(i*increment)])
		translate([-sidel/2,apothemlength(innerradius, teeth),0])
		tooth(sidel, outerradius-innerradius);
	}
}




module winch()
{
	difference()
	{
		union()
		{
			linear_extrude(teethheight)
			gear(winchteeth, winchradius, winchradius+teethlength);
			cylinder(r = winchradius, h = winchheight);
			translate([0,0,teethheight+safety])
			cylinder(r = winchradius+teethlength, h = wallthickness);
		}
		cylinder(r = screwradius, h = winchheight);
		translate([0,0,winchheight-boltheight])
		cylinder(r = boltradius, h = boltheight);
		translate([winchradius*2, screwradius+wallthickness+winchwireholeradius, winchheight/2])
		rotate([0,-90,0])
		cylinder(r = winchwireholeradius, h = winchradius*4);
	}
}


module wincharm()
{
	union()
	{
		cube([wincharmthickness,wincharmlength,teethheight]);
		linear_extrude(teethheight)
		translate([wincharmthickness,wincharmlength/2,0])
		rotate([0,0,270])
		tooth(sidelength(winchradius, winchteeth), teethheight);
		translate([0,wincharmlength/2-sidelength(winchradius, winchteeth)/2,0])
		rotate([0,0,90])
		cube([wincharmthickness,winchpaddlelength,teethheight]);
	}
}



//the assembly plate that the entire winching mech rides on
module winchholder()
{
	translate([winchplatelength/2, winchplatewidth/2, 0])
	translate([-(winchradius+teethheight+wincharmthickness),-wincharmlength/2-screwradius,wallthickness])
	union()
	{
	translate([0,wallthickness,0])
	union()
	{
		translate([0,0,winchwasherheight])
		wincharm();
		translate([winchradius+teethheight+wincharmthickness,wincharmlength/2,winchwasherheight])
		rotate([0,0,8])
		union()
		{
			//winch();
			translate([0,0,-winchwasherheight])
			difference()
			{
				cylinder(r = winchradius, h = winchwasherheight);
				cylinder(r = screwradius, h = winchwasherheight);
			}
		}
	}
	cube([wallthickness*2, wallthickness*2, winchwasherheight+teethheight]);
	translate([0,wincharmlength,0])
	cube([wallthickness*2, wallthickness*2, winchwasherheight+teethheight]);
	}
	//make the plate
	difference()
	{
		cube([winchplatelength, winchplatewidth, wallthickness]);
		translate([winchplatelength/2, winchplatewidth/2, 0])
		cylinder(r = boltradius, h = wallthickness);
	}
	//make the wire holders
	translate([0,winchplatewidth/2, wallthickness])
	difference()
	{
	union()
	{
		cube([wireholeplatethickness, winchplatewidth/2, winchwasherheight+teethheight+wallthickness+winchwireholeradius+wallthickness*2]);
		translate([winchplatelength-wireholeplatethickness,0,0])
		cube([wireholeplatethickness, winchplatewidth/2, winchwasherheight+teethheight+wallthickness+winchwireholeradius+wallthickness*2]);
	}
	translate([0,winchplatewidth/2-winchwireholeradius-wallthickness,winchwasherheight+teethheight+wallthickness])
	rotate([0,90,0])
	cylinder(r = winchwireholeradius, h = winchplatelength);
	}
}

$fn=40;

//translate([winchplatelength/2,winchplatewidth+winchradius+teethlength+wallthickness,0])
winch();
//winchholder();